#lang scribble/manual

@(require "defs.rkt")

@title{Logging In}

@geniusers-only{
The first step is to log in to @(tb).  @clab-only{@(tb) is available to all researchers and educators who work in cloud computing.}
If you have an account at one of its federated facilities, like
@link["https://www.emulab.net"]{Emulab} or
@link["http://geni.net"]{GENI}, then you already have an account at
@(tb).}

@geniusers-only{@section{Using a @(tb) Account}}

If you have signed up for an account at the @(tb) website, simply
open @url[apt-base-url] in your browser, click the ``Log In'' button,
enter your username and password@geniusers-only{, and @bold{skip to the next part of the tutorial}}.

@geniusers-only{
@clab-screenshot["tutorial/front-page.png"]

@section{Using a GENI Account}

If you have an account at the GENI portal, you will use the single-sign-on
features of the @(tb) and GENI portal.

@itemlist[#:style 'ordered

    @instructionstep["Open the web interface"
                     #:screenshot "tutorial/front-page.png"
		     #:screenshot-where "clab"]{
        To log in to @(tb) using a GENI account, start by visiting
        @url[apt-base-url] in your browser and using the ``Log In''
        button in the upper right.
    }

    @instructionstep["Click the \"GENI User\" button"
                     #:screenshot "tutorial/login-page.png"
		     #:screenshot-where "clab"]{
        On the login page that appears, you will use the ``GENI User'' button.
        This will start the GENI authorization tool, which lets you connect
        your GENI account with @(tb). 
    }

    @instructionstep["Select the GENI portal"
                     #:screenshot "tutorial/authorization-tool.png"
		     #:screenshot-where "clab"]{
        You will be asked which facility your account is with. Select the GENI
        icon, which will take you to the GENI portal. There are several other
        facilities that are federated with @(tb), which can be found in the
        drop-down list. 
    }

    @instructionstep["Log into the GENI portal"
                     #:screenshot "tutorial/geni-login.png"
		     #:screenshot-where "clab"]{
        You will be taken to the GENI portal, and will need to select the
        institution that is your identity provider. Usually, this is the
        university you are affiliated with. If your university is not in
        the list, you may need to log in using the ``GENI Project Office''
        identity provider. If you have ever logged in to the GENI portal
        before, your identity provider should be pre-selected; if you
        are not familiar with this login page, there is a good chance that
        you don't have a GENI account and need to 
        @link["http://groups.geni.net/geni/wiki/SignMeUp"]{apply for one}.
    }

    @instructionstep["Log into your institution"
                     #:screenshot "tutorial/utah-login.png"
		     #:screenshot-where "clab"]{
        This will bring you to your institution's standard login page, which you
        likely use to access many on-campus services. (Yours will look different
        from this one.)
    }

    @instructionstep["Authorize CloudLab to use your GENI account"]{
        @margin-note{What's happening in this step is that your browser
        uses your GENI user certificate (which it obtained from the GENI
        portal) to cryptographically sign a
        @link["http://groups.geni.net/geni/wiki/GAPI_AM_API_DRAFT/SpeaksFor"]{``speaks-for'' credential }
        that authorizes the @(tb) portal to make
        @link["http://groups.geni.net/geni/wiki/GeniApi"]{GENI API calls}
        on your behalf.}
        Click the ``Authorize'' button: this will create a signed statement
        authorizing the @(tb) portal to speak on your behalf. This
        authorization is time-limited (see ``Show Advanced'' for the details),
        and all actions the @(tb) portal takes on your behalf are clearly
        traceable.

        If you'd like, you can click ``Remember This Decision'' to skip
        this step in the future (you will still be asked to log in to
        the GENI portal).

        @clab-screenshot["tutorial/authorize.png"]

    }


    @instructionstep["Set up an SSH keypair in the GENI portal"
                     #:screenshot "tutorial/portal-keypair.png"
		     #:screenshot-where "clab"]{
        Though not strictly required, many parts of this tutorial will
        work better if you have an @(ssh) public key associated with your GENI
        Portal account. To upload one, visit
        @link["http://portal.geni.net"]{portal.geni.net} and use the ``SSH
        Keys'' item on the menu that drops down under your name.

        If you are not familiar with @(ssh) keys, you may skip this step. You
        may find
        @link["https://help.github.com/articles/generating-ssh-keys/"]{GitHub's
        @(ssh) key page} helpful in learning how to generate and use them.

        @margin-note{When you create a new experiment, @(tb) grabs the set of @(ssh)
        keys you have set up at the time; any @(ssh) keys that you add later are
        not added to experiments that are already running.} } ]
}
