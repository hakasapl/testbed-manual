#lang scribble/manual

@(require "defs.rkt")

@title[#:tag "ota" #:version apt-version]{@(tb) Over-the-air operation}

This chapter will walk you through the process of instantiating
a @(tb) @seclink["experiments"]{experiment} for over-the-air operation.


@section{Overview}

The @(tb) over-the-air experimental workflow follows the same basic approach described in the @seclink["getting-started"]{getting started} chapter. However, over-the-air operation involves radio frequency (RF) transmissions, that are subject to FCC regulations. The implication of this is that over-the-air experiments cannot be instantiated on-demand. Rather all over-the-air experiments need to @seclink["reservations"]{pre-reserve} all resources that will be needed by the experiment, including spectrum, before the normal experiment instantiation process can be started.

In summary, the steps involved with instantiating an over-the-air experiment in @(tb) are:

@itemlist[
@item{Select a profile you want to instantiate and note the resources required from the profile description.}
@item{Request a reservation for the (sub-)set of resources you need.}
@item{Once your reservation is approved, follow the normal experimental workflow process to instantiate your experiment.}
]

Note: This process is expected to evolve as we gain more experience with over-the-air operation on @(tb).

@section{Spectrum use}

Every @(tb) experiment must declare its spectrum usage in advance: 
while arbitrary reception is possible, transmission on any frequency is
permitted @bold{only by prior permission}.

Program Experimental licencees may submit experiment notifcations
on the FCC OET Experiments System site to operate within the
Salt Lake City Innovation Zone.  Details are available in the
@hyperlink["https://docs.fcc.gov/public/attachments/DA-19-923A1.pdf"]{Public
Notice announcing the Innovation Zone}.

The frequency bands appropriate for experimenter use varies by
node type; more details are available in @seclink["hardware"]{the
hardware chapter}.  The following table provides a summary of frequencies
for currently operational equipment:

@(tabular #:style 'boxed #:sep (hspace 3) (list
    (list "Frequency (GHz)" "Operational node types")
    (list @hyperlink["https://www.fcc.gov/wireless/bureau-divisions/broadband-division/advanced-wireless-services-aws"]{1.7/2.1 AWS (FDD)} (list "Base station " @tt{cellsdr} " nodes; Fixed endpoint " @tt{nuc} " and " @tt{ue} " nodes"))
    (list @hyperlink["https://www.fcc.gov/wireless/bureau-divisions/broadband-division/broadband-radio-service-education-broadband-service"]{2.4-2.6 ISM/BRS (TDD)} "Skylark Wireless Massive MIMO")
    (list @hyperlink["https://www.fcc.gov/wireless/bureau-divisions/mobility-division/35-ghz-band/35-ghz-band-overview"]{3.5 CBRS (TDD)} (list "Base station " @tt{cbrssdr} " nodes"))))

@subsection[#:tag "spectrum-brs-powder"]{BRS Channel Use Information}

@(tb) provides access to 10 MHz of spectrum (2496-2506 MHz) in the BRS range (band 41) for experimental use. Transmissions in this spectrum must adhere to a specific LTE or 5G-NR TDD frame and subframe configuration with downlink (rooftop base stations) and uplink (mobile/fixed endpoints) transmission periods synchronized in time with commercial operators in order to avoid interfering with their adjacent channels. Specifically, transmission from rooftop base stations is disallowed in the period between 1.64323 ms and 3 ms after the beginning of the second as determined by GPS* and repeating every 5 ms thereafter.

The required LTE uplink-downlink frame configuration is number 2 with special subframe configuration 6 (see 3GPP 36.211 and this @hyperlink["https://www.sqimway.com/lte_tdd.php"]{LTE TDD Reference}), which has this arrangement of downlink (D), uplink (U), and special (S) subframes: DSUDD. Each subframe has a duration of 1 ms and the pattern repeats every 5 ms. The special subframe in this pattern includes more time for downlink transmission (DwPTS), followed by a guard period (GP) to allow for time-of-flight differences for downlink reception, followed by more time for uplink transmission (UpPTS) prior to the next uplink subframe. Special subframe configuration 6 sets the durations for DwPTS, GP, and UpPTS as follows:

@itemlist[
@item{DwPTS: 0.64323 ms}
@item{GP:    0.21406 ms}
@item{UpPTS: 0.14271 ms}
]

@screenshot["powder-brs-tdd-config.png"]

For 5G-NR, DDDSUUDDDD with SF27 timing configured as 3:8:3 should be used. The special subframe consists of downlink for 107.14us, guard period for 272.71us and uplink for 107.14us.

* The radios on the rooftops use an Octoclock for their time reference. These Octoclocks are synchronized using WhiteRabbit to a GPS disciplined oscillator. The Octoclock is connected to the 10 MHz and PPS radio inputs. As such you should use the external frequency and time inputs for synchronization.  This is done with a @tt{set_time_source("external")} call via the @tt{multi_usrp} object API.


@section{Step-by-step walkthrough}

The step-by-step walkthrough below will instantiate an over-the-air experiment using the @hyperlink["https://github.com/srsLTE/srsLTE"]{srsLTE} open source software stack. The walkthrough assumes you have an account on @(tb) and have your own project that has been enabled by @(tb) administrators for over-the-air operation. (Specifically, you will not be able to use the "TryPowder" project to instantiate this profile.)

@itemlist[#:style 'ordered
  
	@instructionstep["Log into the portal"
					#:screenshot "powder-website.png"]{
    	Start by pointing your browser at @url[(apturl)] and log in.
  	}

	@instructionstep["Find the resources needed by the ota_srslte profile"
					#:screenshot "powder-ota2.png"]{
		We will use a basic srsLTE profile developed by the @(tb) team. You can find the "ota_srslte" profile at this @hyperlink["https://www.powderwireless.net/p/PowderProfiles/ota_srslte"]{link}. Alternatively you can search for the profile: Select "Experiments" and then "Start Experiment" from the portal. Then select "Change Profile". In the search box enter "ota_srslte" and select the srsLTE OTA profile provided by the @(tb) team.
		
		In the profile description, note the hardware and frequency resources that are required for this profile.
		
		For this activity we will select the following specific resources:
		
		Hardware: Humanities, nuc2; Emulab, cellsdr1-browning; Emulab, d740
		
		Frequency: 2560 MHz to 2570 MHz,  2680 MHz to 2690 MHz 

		}


	@instructionstep["Go to the resource reservation page"
					#:screenshot "powder-reservation1.png"]{
		Within the @(tb) portal, select "Experiments", then select "Reserve Resources" from the drop down menu.
	}

	@instructionstep["Reserve the resources required by the ota_srslte profile"
					#:screenshot "powder-reservation3.png"]{
		Fill out the reservation page to reserve the specific resources identified above. Note that resource reservations are tied to specific projects. You will have to use your own project here, not the @(tb) team project ("PowderTeam") shown in the screenshot. (The "TryPowder" project used for the @seclink["getting-started"]{getting stared} activity also can not be used for over-the-air operation.)
		
		Select a date and time for your reservation and provide a "Reason" description.
		
		Once you have completed all the required fields, select "Check" to see if your reservation request can be accommodated. You might have to adjust your request (select different resources and/or change you reservation time/date) to make it fit. Note that the table/graphs on the right of the Reservation Request page show current frequency reservations and resource availability.  				
	}
	
	@instructionstep["Submit your reservation request"
					#:screenshot "powder-reservation4.png"]{
		If your reservation request can be accommodated you will be presented with a pop-up window to "Submit" the reservation.
		
		Once you have submitted your reservation you will have to wait for the reservation to be approved by @(tb) administrators before proceeding with the rest of this activity (during the time slot requested in your request).   				
	}

	@instructionstep["Select the profile"
					#:screenshot "powder-ota2.png"]{
		You will receive email when your reservation request is "approved" and also when your reservation becomes "active".
		
		When your reservation becomes active you can proceed to instantiate the ota_srslte profile. (Alternatively, you can follow the steps below as soon as your reservation is approved, but in the "Schedule" step, schedule the instantiation of the profile to coincide with your reservation.)
		
		As before: You can find the "ota_srslte" profile at this @hyperlink["https://www.powderwireless.net/p/PowderProfiles/ota_srslte"]{link}. Alternatively you can search for the profile: Select "Experiments" and then "Start Experiment" from the portal. Then select "Change Profile". In the search box enter "ota_srslte" and select the srsLTE OTA profile provided by the @(tb) team.}

	@instructionstep["Select resources for your profile"
					#:screenshot "powder-ota3.png"]{
		In the "Parameterize" step, select the @bold{same} resources you requested/received approval for.
		
		For this profile frequency resources are directly specified/requested by the profile. 				
	}
	
	@instructionstep["Finalize"
					#:screenshot "powder-ota4.png"]{
		In the "Finalize" step, be sure to select the same project under which you submitted the resource reservation request.				
	}

	@instructionstep["Schedule"
					#:screenshot "powder-ota5.png"]{
		In the "Schedule" step, select "Start immediately" (the default) and make sure your "Experiment duration" fits within the time period associated with your reservation.
	}

	@instructionstep["Open a browser shell"
					#:screenshot "powder-ota9.png"]{
		Once your experiment is fully instantiated, i.e., shows "Your experiment is ready!", open a browser shell on the "cellsdr1-browning-comp" node: Select the "List View" tab, click on the gear icon (under "Actions") next to "cellsdr1-browning-comp" and select "Shell" from the drop down menu.
		
		Note these instructions are also available in the "Profile Instructions" tab.
	}

	@instructionstep["Start up srsepc"
					#:screenshot "powder-ota10.png"]{
		In the "cellsdr1-browning-comp" browser shell window, start up the srsLTE EPC:
		
		"sudo srsepc"
	}

	@instructionstep["Start up srsenb"
					#:screenshot "powder-ota11.png"]{
		Open another browser shell on the "cellsdr1-browning-comp" node and start up the srsLTE eNodeB:
		
		"sudo srsenb" 				
	}
	
	@instructionstep["Start up srsue"
					#:screenshot "powder-ota12.png"]{
		Open a browser shell on the "b210-humanities-nuc2" node and start up the srsLTE UE:
		
		"sudo srsue"				
	}
	
	
	@instructionstep["Test UE connectivity"
					#:screenshot "powder-ota13.png"]{
	
		If your UE successfully connects to the LTE network, a "tun_srsue" interface will be created on the "b210-humanities-nuc2" node and you can verify end-to-end connectivity across your LTE network:
		
		Open another browser shell on the "b210-humanities-nuc2" node and "ping" the SGi IP address via your RF link:
		
		"ping 172.16.0.1"
					
	}

	@instructionstep["ssh/X1 and GUI"
					#:screenshot "powder-ota15.png"]{
		If you have ssh and X11	enabled on your laptop/desktop, you can follow the same step described above, but instead do it via ssh shell terminals. (Select the "List View" tab, and select the ssh command (in black) next to the node you want to create a shell on. (Or run the shown ssh command from your ssh client.))
		
		In this case run the UE with GUI enabled to get a "softscope" associated with your RF link:
		
		"sudo srsue --gui.enable 1"			
	}

]

@section{Next Steps}

The @seclink["roadmap"]{@(tb) roadmap} section contains useful next steps.

