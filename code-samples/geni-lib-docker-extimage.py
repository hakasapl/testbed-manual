"""An example of a Docker container running an external, unmodified image."""

import geni.portal as portal
import geni.rspec.pg as rspec

request = portal.context.makeRequestRSpec()
node = request.DockerContainer("node")
node.docker_extimage = "ubuntu:16.04"
portal.context.printRequestRSpec()
