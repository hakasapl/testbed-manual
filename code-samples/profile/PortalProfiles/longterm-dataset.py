"""This profile demonstrates how to use a remote dataset on your node, either a
long term dataset or a short term dataset, created via the Portal. 

Instructions:
Log into your node, your dataset file system in mounted at `/mydata`.
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as rspec
# Import the emulab extensions library.
import geni.rspec.emulab

# Create a Request object to start building the RSpec.
request = portal.context.makeRequestRSpec()

# Add a node to the request.
node = request.RawPC("node")
# We need a link to talk to the remote file system, so make an interface.
iface = node.addInterface()

# The remote file system is represented by special node.
fsnode = request.RemoteBlockstore("fsnode", "/mydata")
# This URN is displayed in the web interfaace for your dataset.
fsnode.dataset = "urn:publicid:IDN+emulab.net:portalprofiles+ltdataset+DemoDataset"
#
# The "rwclone" attribute allows you to map a writable copy of the
# indicated SAN-based dataset. In this way, multiple nodes can map
# the same dataset simultaneously. In many situations, this is more
# useful than a "readonly" mapping. For example, a dataset
# containing a Linux source tree could be mapped into multiple
# nodes, each of which could do its own independent,
# non-conflicting configure and build in their respective copies.
# Currently, rwclones are "ephemeral" in that any changes made are
# lost when the experiment mapping the clone is terminated.
#
#fsnode.rwclone = True

#
# The "readonly" attribute, like the rwclone attribute, allows you to
# map a dataset onto multiple nodes simultaneously. But with readonly,
# those mappings will only allow read access (duh!) and any filesystem
# (/mydata in this example) will thus be mounted read-only. Currently,
# readonly mappings are implemented as clones that are exported
# allowing just read access, so there are minimal efficiency reasons to
# use a readonly mapping rather than a clone. The main reason to use a
# readonly mapping is to avoid a situation in which you forget that
# changes to a clone dataset are ephemeral, and then lose some
# important changes when you terminate the experiment.
#
#fsnode.readonly = True

# Now we add the link between the node and the special node
fslink = request.Link("fslink")
fslink.addInterface(iface)
fslink.addInterface(fsnode.interface)

# Special attributes for this link that we must use.
fslink.best_effort = True
fslink.vlan_tagging = True

# Print the RSpec to the enclosing page.
portal.context.printRequestRSpec()