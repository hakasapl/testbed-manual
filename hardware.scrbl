#lang scribble/manual
@(require "defs.rkt")

@title[#:tag "hardware" #:version apt-version]{Hardware}

@(tb) can allocate experiments on any one of several federated clusters.

@clab-only{
    @(tb) has the ability to dispatch experiments to several clusters: three
    that belong to CloudLab itself, plus several more that belong to federated
    projects.

    Additional hardware expansions are planned, and descriptions of them
    can be found at @url[(@apturl "hardware.php")]
}

@clab-only{
    @; Reminder! This is duplicated below for Apt
    @section[#:tag "cloudlab-utah"]{CloudLab Utah}

    The CloudLab cluster at the University of Utah is being built in partnership
    with HP and Dell.
    It currently consists of 4 Intel Ice Lake servers,
    208 AMD EPYC Rome servers (two generations),
    200 Intel Xeon E5 servers,
    270 Xeon-D servers, and 270 64-bit ARM servers
    for a total of 9,636 cores.
    The cluster is housed in the University of Utah's Downtown Data
    Center in Salt Lake City.

    @(nodetype "m400" 270 "64-bit ARM"
        (list "CPU"  "Eight 64-bit ARMv8 (Atlas/A57) cores at 2.4 GHz (APM X-GENE)")
        (list "RAM"  "64GB ECC Memory (8x 8 GB DDR3-1600 SO-DIMMs)")
        (list "Disk" "120 GB of flash (SATA3 / M.2, Micron M500)")
        (list "NIC"  "Dual-port Mellanox ConnectX-3 10 GB NIC (PCIe v3.0, 8 lanes")
    )

    @(nodetype "m510" 270 "Intel Xeon-D"
        (list "CPU"  "Eight-core Intel Xeon D-1548 at 2.0 GHz")
        (list "RAM"  "64GB ECC Memory (4x 16 GB DDR4-2133 SO-DIMMs)")
        (list "Disk" "256 GB NVMe flash storage")
        (list "NIC"  "Dual-port Mellanox ConnectX-3 10 GB NIC (PCIe v3.0, 8 lanes")
    )

    There are 45 nodes in a chassis, and this cluster consists of twelve
    chassis. Each chassis has two 45XGc switches; each node is connected to
    both switches, and each chassis switch has four 40Gbps uplinks, for a total
    of 320Gbps of uplink capacity from each chassis. One switch is used for
    control traffic, connecting to the Internet, etc. The other is used to
    build experiment topologies, and should be used for most experimental
    purposes.

    All chassis are interconnected through a large HP FlexFabric 12910 switch
    which has full bisection bandwidth internally.

    We have plans to enable some users to allocate entire chassis; when
    allocated in this mode, it will be possible to have complete administrator
    control over the switches in addition to the nodes.

    In phase two we added 50 Apollo R2200 chassis each with four HPE ProLiant
    XL170r server modules. Each server has 10 cores for a total of 2000 cores.

    @(nodetype "xl170" 200 "Intel Broadwell, 10 core, 1 disk"
        (list "CPU"  "Ten-core Intel E5-2640v4 at 2.4 GHz")
        (list "RAM"  "64GB ECC Memory (4x 16 GB DDR4-2400 DIMMs)")
        (list "Disk" "Intel DC S3520 480 GB 6G SATA SSD")
        (list "NIC"  "Two Dual-port Mellanox ConnectX-4 25 GB NIC (PCIe v3.0, 8 lanes")
    )

    Each server is connected via a 10Gbps control link (Dell switches) and a
    25Gbps experimental link to Mellanox 2410 switches in groups of 40 servers.
    Each of the five groups' experimental switches are connected to a Mellanox
    2700 spine switch at 5x100Gbps. That switch in turn interconnects with the
    rest of the Utah CloudLab cluster via 6x40Gbps uplinks to the HP FlexFabric
    12910 switch.

    A unique feature of the phase two nodes is the addition of eight ONIE
    bootable "user allocatable" switches that can run a variety of Open Network
    OSes: six Dell S4048-ONs and two Mellanox MSN2410-BB2Fs. These switches and
    all 200 nodes are connected to two NetScout 3903 layer-1 switches, allowing
    flexible combinations of nodes and switches in an experiment.

    For phase two we also added 28 Dell AMD EPYC-based servers with dual 100Gb
    Ethernet ports.

    @(nodetype "d6515" 28 "AMD EPYC Rome, 32 core, 2 disk, 100Gb Ethernet"
        (list "CPU"  "32-core AMD 7452 at 2.35GHz")
        (list "RAM"  "128GB ECC Memory (8x 16 GB 3200MT/s RDIMMs)")
        (list "Disk" "Two 480 GB 6G SATA SSD")
        (list "NIC"  "Dual-port Mellanox ConnectX-5 100 GB NIC (PCIe v4.0)")
        (list "NIC"  "Dual-port Broadcom 57414 25 GB NIC")
    )

    Each server is connected via a 25Gbps control link (Dell S5224F switch),
    2 x 100Gbs experiment links (Dell Z9264F-ON switch), and a 25Gbps
    experiment link (Dell S5248F-ON switch).  The experiment switches are
    connected to the "phase two" Mellanox 2700 spine switch at 4x100Gbps
    (Z9264F) and 2x100Gbps (S5248F).

    In the initial installment of phase three (2021) we added 180 more AMD EPYC
    Rome servers in two configurations.

    @(nodetype "c6525-25g" 144 "AMD EPYC Rome, 16 core, 2 disk, 25Gb Ethernet"
        (list "CPU"  "16-core AMD 7302P at 3.00GHz")
        (list "RAM"  "128GB ECC Memory (8x 16 GB 3200MT/s RDIMMs)")
        (list "Disk" "Two 480 GB 6G SATA SSD")
        (list "NIC"  "Two dual-port Mellanox ConnectX-5 25Gb GB NIC (PCIe v4.0)")
    )

    @(nodetype "c6525-100g" 36 "AMD EPYC Rome, 24 core, 2 disk, 25/100Gb Ethernet"
        (list "CPU"  "24-core AMD 7402P at 2.80GHz")
        (list "RAM"  "128GB ECC Memory (8x 16 GB 3200MT/s RDIMMs)")
        (list "Disk" "Two 1.6 TB NVMe SSD (PCIe v4.0)")
        (list "NIC"  "Dual-port Mellanox ConnectX-5 25 GB NIC (PCIe v4.0)")
        (list "NIC"  "Dual-port Mellanox ConnectX-5 Ex 100 GB NIC (PCIe v4.0)")
    )

    The "-25g" variant nodes have 2 x 25Gb experiment links (Dell S5296F switches)
    and SATA-based SSDs, and are intended for general experimentation.

    The "-100g" variant nodes have one 25Gb (Dell S5296) and one 100Gb (Dell Z9264)
    experiment link as well as two large NVMe-based SSDs and more cores, and are
    intended for network and storage intensive experimentation.

    Each server is also connected via a 25Gbps control link (Dell S5296F switch).

    The experiment switches are interconnected via a single Dell Z9332 using
    4-8 100Gb links each.

    In the second installment of phase three (early 2022), we added a small set of
    "expandable" nodes, 2U boxes with multiple PCIe slots available for add in
    devices such as GPUs, FPGA, or other accelerator cards.

    @(nodetype "d750" 4 "Intel Ice Lake, 16 core, 2 disk, 25Gb Ethernet"
        (list "CPU"  "16-core Intel Xeon Gold 6326 at 2.90GHz")
        (list "RAM"  "128GB ECC Memory (16x 8 GB 3200MT/s RDIMMs)")
        (list "Disk" "480 GB SATA SSD (PCIe v4.0)")
        (list "Disk" "400 GB NVMe Optane P5800X SSD (PCIe v4.0)")
        (list "NIC"  "Quad-port BCM57504 NetXtreme-E 25 GB NIC")
    )

    Each server is also connected via a 25Gbps control link (Dell S5296F switch)
    and three 25Gbps experiment links (via another Dell S5296F switch).

    These machines have four available full-length double-wide PCIe v4 x16 slots
    and 2400W power supplies capable of handling four enterprise GPUs or other
    accelerator cards.

    They also have a 400GB Optane write-intensive SSD providing another level
    of storage hierarchy for experimentation.

    The Utah Cloudlab cluster includes a storage server for remote datasets.
    The server currently has 80TB available for allocation.
}

@clab-only{
    @section[#:tag "cloudlab-wisconsin"]{CloudLab Wisconsin}

    The CloudLab cluster at the University of Wisconsin is built in
    partnership with Cisco, Seagate, and HP. The cluster, which is in
    Madison, Wisconsin, has 523 servers with a total of 10,060 cores connected
    in a CLOS topology with full bisection bandwidth. It has 1,396 TB of
    storage, including SSDs on every node. 

    More technical details can be found at @url[(@apturl "hardware.php#wisconsin")]

    @(nodetype "c220g1" 90 "Haswell, 16 core, 3 disks"
        (list "CPU"  "Two Intel E5-2630 v3 8-core CPUs at 2.40 GHz (Haswell w/ EM64T)")
        (list "RAM"  "128GB ECC Memory (8x 16 GB DDR4 1866 MHz dual rank RDIMMs)")
        (list "Disk" "Two 1.2 TB 10K RPM 6G SAS SFF HDDs")
        (list "Disk" "One Intel DC S3500 480 GB 6G SATA SSDs")
        (list "NIC"  "Dual-port Intel X520-DA2 10Gb NIC (PCIe v3.0, 8 lanes)")
        (list "NIC"  "Onboard Intel i350 1Gb"))

    @(nodetype "c240g1" 10 "Haswell, 16 core, 14 disks"
        (list "CPU"  "Two Intel E5-2630 v3 8-core CPUs at 2.40 GHz (Haswell w/ EM64T)")

        (list "RAM"  "128GB ECC Memory (8x 16 GB DDR4 1866 MHz dual rank RDIMMs)")

        (list "Disk" "Two Intel DC S3500 480 GB 6G SATA SSDs")
        (list "Disk" "Twelve 3 TB HDDs donated by Seagate")
        (list "NIC"  "Dual-port Intel X520-DA2 10Gb NIC (PCIe v3.0, 8 lanes)")
        (list "NIC"  "Onboard Intel i350 1Gb"))

    @(nodetype "c220g2" 163 "Haswell, 20 core, 3 disks"
        (list "CPU"  "Two Intel E5-2660 v3 10-core CPUs at 2.60 GHz (Haswell EP)")
        (list "RAM"  "160GB ECC Memory (10x 16 GB DDR4 2133 MHz dual rank RDIMMs)")
        (list "Disk" "One Intel DC S3500 480 GB 6G SATA SSDs")
        (list "Disk" "Two 1.2 TB 10K RPM 6G SAS SFF HDDs")
        (list "NIC"  "Dual-port Intel X520 10Gb NIC (PCIe v3.0, 8 lanes")
        (list "NIC"  "Onboard Intel i350 1Gb"))

    @(nodetype "c240g2" 4 "Haswell, 20 core, 8 disks"
        (list "CPU"  "Two Intel E5-2660 v3 10-core CPUs at 2.60 GHz (Haswell EP)")
        (list "RAM"  "160GB ECC Memory (10x 16 GB DDR4 2133 MHz dual rank RDIMMs)")
        (list "Disk" "Two Intel DC S3500 480 GB 6G SATA SSDs")
        (list "Disk" "Two 1TB HDDs")
        (list "Disk" "Four 3TB HDDs")
        (list "NIC"  "Dual-port Intel X520 10Gb NIC (PCIe v3.0, 8 lanes")
        (list "NIC"  "Onboard Intel i350 1Gb"))

    Phase two added 260 new nodes, 36 with one or more GPUs:

    @(nodetype "c220g5" 224 "Intel Skylake, 20 core, 2 disks"
        (list "CPU"  "Two Intel Xeon Silver 4114 10-core CPUs at 2.20 GHz")
        (list "RAM"  "192GB ECC DDR4-2666 Memory")
        (list "Disk" "One 1 TB 7200 RPM 6G SAS HDs")
        (list "Disk" "One Intel DC S3500 480 GB 6G SATA SSD")
        (list "NIC"  "Dual-port Intel X520-DA2 10Gb NIC (PCIe v3.0, 8 lanes)")
        (list "NIC"  "Onboard Intel i350 1Gb"))

    @(nodetype "c240g5" 32 "Intel Skylake, 20 core, 2 disks, GPU"
        (list "CPU"  "Two Intel Xeon Silver 4114 10-core CPUs at 2.20 GHz")
        (list "RAM"  "192GB ECC DDR4-2666 Memory")
        (list "Disk" "One 1 TB 7200 RPM 6G SAS HDs")
        (list "Disk" "One Intel DC S3500 480 GB 6G SATA SSD")
        (list "GPU"  "One NVIDIA 12GB PCI P100 GPU")
        (list "NIC"  "Dual-port Intel X520-DA2 10Gb NIC (PCIe v3.0, 8 lanes)")
        (list "NIC"  "Onboard Intel i350 1Gb"))

    @(nodetype "c4130" 4 "Intel Broadwell, 16 core, 2 disks, 4 GPUs"
        (list "CPU"  "Two Intel Xeon E5-2667 8-core CPUs at 3.20 GHz")
        (list "RAM"  "128GB ECC Memory")
        (list "Disk" "Two 960 GB 6G SATA SSD")
        (list "GPU"  "Four NVIDIA 16GB Tesla V100 SMX2 GPUs"))

    All nodes are connected to two networks:

    @itemlist[
        @item{A 1 Gbps Ethernet @bold{``control network''}---this network 
            is used for remote access, experiment management, etc., and is
            connected to the public Internet. When you log in to nodes in your
            experiment using @code{ssh}, this is the network you are using.
            @italic{You should not use this network as part of the experiments
            you run in @(tb).}
        }

        @item{A 10 Gbps Ethernet @bold{``experiment network''}--each node has
            one or two interfaces on this network. The nodes are directly connected
	    to a number of HP and Dell leaf switches which in turn are connected
	    to two interconnected spine switches.
        }
    ]

    The Wisconsin Cloudlab cluster includes a storage server for remote datasets.
    The server currently has 30TB available for allocation.
}

@clab-only{
    @section[#:tag "cloudlab-clemson"]{CloudLab Clemson}

    The CloudLab cluster at Clemson University has been built
    partnership with Dell. The cluster so far has 345
    servers with a total of 12,160 cores, 1,450TB of disk space, and
    97TB of RAM. All nodes have at least 10GB Ethernet (some 25Gb or 100Gb)
    and some have QDR Infiniband.  It is located in Clemson, South Carolina.

    More technical details can be found at @url[(@apturl "hardware.php#clemson")]

    @(nodetype "c8220" 96 "Ivy Bridge, 20 core"
        (list "CPU"  "Two Intel E5-2660 v2 10-core CPUs at 2.20 GHz (Ivy Bridge)")
        (list "RAM"  "256GB ECC Memory (16x 16 GB DDR4 1600MT/s dual rank RDIMMs")

        (list "Disk" "Two 1 TB 7.2K RPM 3G SATA HDDs")
        (list "NIC"  "Dual-port Intel 10Gbe NIC (PCIe v3.0, 8 lanes")
        (list "NIC"  "Qlogic QLE 7340 40 Gb/s Infiniband HCA (PCIe v3.0, 8 lanes)"))

    @(nodetype "c8220x" 4 "Ivy Bridge, 20 core, 20 disks"
        (list "CPU"  "Two Intel E5-2660 v2 10-core CPUs at 2.20 GHz (Ivy Bridge)")
        (list "RAM"  "256GB ECC Memory (16x 16 GB DDR4 1600MT/s dual rank RDIMMs")

        (list "Disk" "Eight 1 TB 7.2K RPM 3G SATA HDDs")
        (list "Disk" "Twelve 4 TB 7.2K RPM 3G SATA HDDs")
        (list "NIC"  "Dual-port Intel 10Gbe NIC (PCIe v3.0, 8 lanes")
        (list "NIC"  "Qlogic QLE 7340 40 Gb/s Infiniband HCA (PCIe v3.0, 8 lanes)"))

    @(nodetype "c6320" 84 "Haswell, 28 core"
        (list "CPU"  "Two Intel E5-2683 v3 14-core CPUs at 2.00 GHz (Haswell)")
        (list "RAM"  "256GB ECC Memory")
        (list "Disk" "Two 1 TB 7.2K RPM 3G SATA HDDs")
        (list "NIC"  "Dual-port Intel 10Gbe NIC (X520)")
        (list "NIC"  "Qlogic QLE 7340 40 Gb/s Infiniband HCA (PCIe v3.0, 8 lanes)"))

    @(nodetype "c4130" 2 "Haswell, 28 core, two GPUs"
        (list  "CPU"  "Two Intel E5-2680 v3 12-core processors at 2.50 GHz (Haswell)")
        (list "RAM"  "256GB ECC Memory")
        (list "Disk" "Two 1 TB 7.2K RPM 3G SATA HDDs")
        (list "GPU"  "Two Tesla K40m GPUs")
        (list "NIC"  "Dual-port Intel 1Gbe NIC (i350)")
        (list "NIC"  "Dual-port Intel 10Gbe NIC (X710)")
        (list "NIC"  "Qlogic QLE 7340 40 Gb/s Infiniband HCA (PCIe v3.0, 8 lanes)"))

    There are also two, storage intensive (270TB each!) nodes
    that should only be used if you need a huge amount of volatile
    storage. These nodes have only 10GB Ethernet.

    @(nodetype "dss7500" 2 "Haswell, 12 core, 270TB disk"
        (list "CPU"  "Two Intel E5-2620 v3 6-core CPUs at 2.40 GHz (Haswell)")
        (list "RAM"  "128GB ECC Memory")
        (list "Disk" "Two 120 GB 6Gbps SATA SSDs")
        (list "Disk" "45 6 TB 7.2K RPM 6Gbps SATA HDDs")
        (list "NIC"  "Dual-port Intel 10Gbe NIC (X520)"))

    There are three networks at the Clemson site:

    @itemlist[
        @item{A 1 Gbps Ethernet @bold{``control network''}---this network 
            is used for remote access, experiment management, etc., and is
            connected to the public Internet. When you log in to nodes in your
            experiment using @code{ssh}, this is the network you are using.
            @italic{You should not use this network as part of the experiments
            you run in @(tb).}
        }

        @item{A 10 Gbps Ethernet @bold{``experiment network''}--each node has
            @bold{one interface} on this network.  This network is implemented
            using three Force10 S6000 and three Force10 Z9100 switches.  Each
            S6000 switch is connected to a companion Z9100 switch via a 480Gbps
            link aggregate.
        }

        @item{A 40 Gbps QDR Infiniband @bold{``experiment network''}--each
            node has one connection to this
            network, which is implemented using a large Mellanox chassis switch
            with full bisection bandwidth.
        }
    ]

    Phase two added 18 Dell C6420 chassis each with four dual-socket Skylake-based
    servers. Each of the 72 servers has 32 cores for a total of 2304 cores.

    @(nodetype "c6420" 72 "Intel Skylake, 32 core, 2 disk"
        (list "CPU"  "Two Sixteen-core Intel Xeon Gold 6142 CPUs at 2.6 GHz")
        (list "RAM"  "384GB ECC DDR4-2666 Memory")
        (list "Disk" "Two Seagate 1TB 7200 RPM 6G SATA HDs")
        (list "NIC"  "Dual-port Intel X710 10Gbe NIC")
    )

    Each server is connected via a 1Gbps control link (Dell D3048 switches) and a
    10Gbps experimental link (Dell S5048 switches).

    These Phase two machines do not include Infiniband.

    Phase two also added 6 @link["https://www.ibm.com/support/knowledgecenter/en/POWER8/p8hdx/8335_gtb_landing.htm"]{IBM Power System S822LC (8335-GTB)} POWER8 servers.  These machines are booted using the Linux-based @link["https://github.com/open-power/skiboot"]{OpenPOWER firmware (OPAL)}.  They can run code in either little- or big-endian modes, but we only provide a little-endian standard system image (`UBUNTU18-PPC64LE`).

    @(nodetype "ibm8335" 6 "POWER8NVL, 20 core, 256GB RAM, 1 GPU"
    	(list "CPU"  "Two ten-core (8 threads/core) IBM POWER8NVL CPUs at 2.86 GHz")
	(list "RAM"  "256GB 1600MHz DDR4 memory")
	(list "Disk" "Two Seagate 1TB 7200 RPM 6G SATA HDDs (ST1000NX0313)")
	(list "NIC"  "One Broadcom NetXtreme II BCM57800 1/10 GbE NIC")
	(list "GPU"  "Two NVIDIA GP100GL (Tesla P100 SMX2 16GB)")
	(list "FPGA" "One ADM-PCIE-KU3 (Xilinx Kintex UltraScale)")
    )

    You can find more info @link["https://www.redbooks.ibm.com/redpapers/pdfs/redp5405.pdf"]{here}.

    Phase three added 15 Dell R7525 servers, each with dual-core AMD EPYX
    processors, two NVIDIA GPUs, and a Mellanox BlueField2 SmartNIC.

    @(nodetype "r7525" 15 "AMD EPYC Rome, 64 core, 512GB RAM, 2 x GPU"
        (list "CPU"  "Two 32-core AMD 7542 at 2.9GHz")
        (list "RAM"  "512GB ECC Memory (16x 32 GB 3200MHz DDR4)")
        (list "Disk" "One 2TB 7200 RPM 6G SATA HDD")
        (list "NIC"  "Dual-port Mellanox ConnectX-5 25 Gb NIC (PCIe v4.0)")
        (list "NIC"  "Dual-port Mellanox BlueField2 100 Gb SmartNIC")
        (list "GPU"  "Two NVIDIA GV100GL (Tesla V100S PCIe 32GB)")
    )

    The nodes have a 1Gb control network connection, one 25Gb experiment
    connection, and 2 x 100Gb connections via the BlueField2 card.

    The latest addition to the cluster includes 64 new machines.

    @(nodetype "r650" 32 "Intel Ice Lake, 72 core, 256GB RAM, 1.6TB NVMe"
        (list "CPU"  "Two 36-core Intel Xeon Platinum 8360Y at 2.4GHz")
        (list "RAM"  "256GB ECC Memory (16x 16 GB 3200MHz DDR4)")
        (list "Disk" "One 480GB SATA SSD")
        (list "Disk" "One 1.6TB NVMe SSD (PCIe v4.0)")
        (list "NIC"  "Dual-port Mellanox ConnectX-5 25 Gb NIC (PCIe v4.0)")
        (list "NIC"  "Dual-port Mellanox ConnectX-6 100 Gb NIC (PCIe v4.0)")
    )

    @(nodetype "r6525" 32 "AMD EPYC Milan, 64 core, 256GB RAM, 1.6TB NVMe"
        (list "CPU"  "Two 32-core AMD 7543 at 2.8GHz")
        (list "RAM"  "256GB ECC Memory (16x 16 GB 3200MHz DDR4)")
        (list "Disk" "One 480GB SATA SSD")
        (list "Disk" "One 1.6TB NVMe SSD (PCIe v4.0)")
        (list "NIC"  "Dual-port Mellanox ConnectX-5 25 Gb NIC (PCIe v4.0)")
        (list "NIC"  "Dual-port Mellanox ConnectX-6 100 Gb NIC (PCIe v4.0)")
    )

    Each of these servers is connected via a 25Gbps control link and a
    100Gbps experimental link.

    The Clemson Cloudlab cluster includes a storage server for remote datasets.
    The server currently has 40TB available for allocation.
}

@section[#:tag "apt-cluster"]{Apt Cluster}

@apt-only{@margin-note{This is the cluster that is currently used by default
    for all experiments on Apt.}}

The main Apt cluster is housed in the University of Utah's Downtown Data
Center in Salt Lake City, Utah. It contains two classes of nodes:

@(nodetype "r320" 128 "Sandy Bridge, 8 cores"
    (list "CPU"   "1x Xeon E5-2450 processor (8 cores, 2.1Ghz)")
    (list "RAM"   "16GB Memory (4 x 2GB RDIMMs, 1.6Ghz)")
    (list "Disks" "4 x 500GB 7.2K SATA Drives (RAID5)")
    (list "NIC"   "1GbE Dual port embedded NIC (Broadcom)")
    (list "NIC"   "1 x Mellanox MX354A Dual port FDR CX3 adapter w/1 x QSA adapter")
)

@(nodetype "c6220" 64 "Ivy Bridge, 16 cores"
    (list "CPU"   "2 x Xeon E5-2650v2 processors (8 cores each, 2.6Ghz)")
    (list "RAM"   "64GB Memory (8 x 8GB DDR-3 RDIMMs, 1.86Ghz)")
    (list "Disks" "2 x 1TB SATA 7.2K RPM hard drives")
    (list "NIC"   "4 x 1GbE embedded Ethernet Ports (Broadcom)")
    (list "NIC"   "1 x Intel X520 PCIe Dual port 10Gb Ethernet NIC")
    (list "NIC"   "1 x Mellanox FDR CX3 Single port mezzanine card")
)

All nodes are connected to three networks with @bold{one interface each}:

@itemlist[
    @item{A 1 Gbps @italic{Ethernet} @bold{``control network''}---this network
        is used for remote access, experiment management, etc., and is
        connected to the public Internet. When you log in to nodes in your
        experiment using @code{ssh}, this is the network you are using.
        @italic{You should not use this network as part of the experiments you
        run in Apt.}
    }

    @item{A @bold{``flexible fabric''} that can run up to 56 Gbps and runs
        @italic{either FDR Infiniband or Ethernet}. This fabric uses NICs and
        switches with @hyperlink["http://www.mellanox.com/"]{Mellanox's}
        VPI technology. This means that we can, on demand, configure each
        port to be either FDR Inifiniband or 40 Gbps (or even non-standard
        56 Gbps) Ethernet. This fabric consists of seven edge switches
        (Mellanox SX6036G) with 28 connected nodes each. There are two core
        switches (also SX6036G), and each edge switch connects to both cores
        with a 3.5:1 blocking factor. This fabric is ideal if you need
        @bold{very low latency, Infiniband, or a few, high-bandwidth Ethernet
        links}.
     }

    @item{A 10 Gbps @italic{Ethernet} @bold{``commodity fabric''}.  One the
        @code{r320} nodes, a port on the Mellanox NIC (permanently set to
        Ethernet mode) is used to connect to this fabric; on the @code{c6220}
        nodes, a dedicated Intel 10 Gbps NIC is used. This fabric is built 
        from two Dell Z9000 switches, each of which has 96 nodes connected
        to it. It is idea for creating @bold{large LANs}: each of the two
        switches has full bisection bandwidth for its 96 ports, and there is a
        3.5:1 blocking factor between the two switches.
    }
        
]

There is no remote dataset capability at the Apt cluster.

@; Total hack, I don't want this cluster listed first for Apt
@apt-only{
    @section[#:tag "cloudlab-utah"]{CloudLab Utah}

    @margin-note{This cluster is part of
    @link["https://www.cloudlab.us"]{CloudLab}, but is also available to Apt
    users.}

    The CloudLab cluster at the University of Utah is being built in partnership
    with HP. The first phase of this cluster consists of 315 64-bit ARM
    servers with 8 cores each, for a total of 2,520 cores. The servers are
    built on HP's Moonshot platform using X-GENE system-on-chip designs from
    Applied Micro. The cluster is hosted in the University of Utah's Downtown
    Data Center in Salt Lake City.

    More technical details can be found at @url[(@apturl "hardware.php#utah")]

    @(nodetype "m400" 315 "64-bit ARM"
        (list "CPU"  "Eight 64-bit ARMv8 (Atlas/A57) cores at 2.4 GHz (APM X-GENE)")
        (list "RAM"  "64GB ECC Memory (8x 8 GB DDR3-1600 SO-DIMMs)")
        (list "Disk" "120 GB of flash (SATA3 / M.2, Micron M500)")
        (list "NIC"  "Dual-port Mellanox ConnectX-3 10 GB NIC (PCIe v3.0, 8 lanes")
    )

    There are 45 nodes in a chassis, and this cluster consists of seven
    chassis. Each chassis has two 45XGc switches; each node is connected to
    both switches, and each chassis switch has four 40Gbps uplinks, for a total
    of 320Gbps of uplink capacity from each chassis. One switch is used for
    control traffic, connecting to the Internet, etc. The other is used to
    build experiment topologies, and should be used for most experimental
    purposes.

    All chassis are interconnected through a large HP FlexFabric 12910 switch
    which has full bisection bandwidth internally.

    We have plans to enable some users to allocate entire chassis; when
    allocated in this mode, it will be possible to have complete administrator
    control over the switches in addition to the nodes.
}

@clab-only{
    @section[#:tag "mass"]{Mass}

    UMass and the Mass Open Cloud host a cluster at the
    @link["https://www.mghpcc.org/"]{Massachusetts Green
    High Performance Compute Center} in Holyoke, Massachusetts.

    @(nodetype "rs440" 5 "Skylake, 32 cores"
        (list "CPU"   "2 x Xeon Gold 6130 processors (16 cores each, 2.1Ghz)")
        (list "RAM"   "192GB Memory (12 x 16GB RDIMMs)")
        (list "Disks" "1 x 240GB SATA SSD drives")
        (list "NIC"   "2 x 10GbE embedded Ethernet Ports (Broadcom 57412)")
    )

    These nodes are connected via two 10Gbps ports to a Dell S4048-ON switch.
    One port is used for control traffic and connectivity to the public
    Internet, and the other is used for the experiment network.

    @(nodetype "rs620" 38 "Sandy Bridge, 16 or 20 cores"
        (list "CPU"   "2 x Xeon processors (8-10 cores each, 2.2Ghz or more)")
        (list "RAM"   "128-384GB Memory (most have 256GB)")
        (list "Disks" "1 x 900GB 10K SAS Drive")
	(list "NIC"   "1GbE Quad port embedded NIC (Intel)")
        (list "NIC"   "1 x Solarflare Dual port SFC9120 10G Ethernet NIC")
    )

    @(nodetype "rs630" 38 "Haswell, 20 cores"
        (list "CPU"   "2 x Xeon E5-2660 v3 processors (10 cores each, 2.6Ghz or more)")
        (list "RAM"   "256GB Memory (16 x 16GB DDR4 DIMMs)")
        (list "Disks" "1 x 900GB 10K SAS Drive")
	(list "NIC"   "1GbE Quad port embedded NIC (Intel)")
        (list "NIC"   "1 x Solarflare Dual port SFC9120 10G Ethernet NIC")
    )

    There is some variation within the @tt{rs620} and @tt{rs630} nodes,
    primarily with the CPUs.

    On these nodes, the control/Internet connections is a 1Gbps port
    and one of the 10Gbps interfaces on each node is used for the
    experiment network.

    There is currently no remote dataset capability at the UMass cluster.
}

@clab-only{
    @section[#:tag "onelab"]{OneLab}

    The @link["https://www.onelab.eu/"]{OneLab} facility at Sorbonne University
    in Paris hosts a small cluster modeled after part of the Utah hardware, with
    one chassis of ARM64 servers. In addition to this cluster, which is available
    to all @(tb) users through the @(tb) interface, OneLab hosts a large number of
    other experiment environments, including clusters, IoT devices, and software
    defined networks. See the @link["https://onelab.eu/services"]{OneLab website}
    for a complete list.

    @(nodetype "m400" 45 "64-bit ARM"
        (list "CPU"  "Eight 64-bit ARMv8 (Atlas/A57) cores at 2.4 GHz (APM X-GENE)")
        (list "RAM"  "64GB ECC Memory (8x 8 GB DDR3-1600 SO-DIMMs)")
        (list "Disk" "120 GB of flash (SATA3 / M.2, Micron M500)")
        (list "NIC"  "Dual-port Mellanox ConnectX-3 10 GB NIC (PCIe v3.0, 8 lanes")
    )

    There is no remote dataset capability at the OneLab cluster.
}
