#lang scribble/manual
@(require "defs.rkt")

@title[#:tag "status-notes" #:version apt-version]{@(tb) Status Notes}

@(tb) is @bold{open}! The @seclink["hardware"]{hardware that is currently
deployed} our Phase I deployment and most of Phase II, and we are still
constantly adding @seclink["planned"]{new features}.
