#lang scribble/manual
@(require racket/date)
@(require "defs.rkt")

@title[#:version apt-version
       #:date (date->string (current-date))]{The PhantomNet Manual}

@author[
    "Jacobus (Kobus) Van der Merwe" "Robert Ricci" "Leigh Stoller" "Kirk Webb" "Jon Duerig" "Gary Wong" "Keith Downie" "Mike Hibler" "Eric Eide"
]

@;{
@italic[(if (equal? (doc-mode) 'pdf)
    (list "The HTML version of this manual is available at " (hyperlink apt-doc-url apt-doc-url))
    (list "This manual is also available as a " (hyperlink "http://docs.phantomnet.org/manual.pdf" "PDF")))]
}

PhantomNet is a mobility testbed, providing researchers with a set of
hardware and software resources that they can use to develop, debug,
and evaluate their mobility designs. Resources available in PhantomNet
include EPC/EPS software (OpenEPC), hardware access points (ip.access
enodeb), PC nodes with mobile radios (HUAWEI cellular modems), and a
large set of commodity bare metal nodes, virtual nodes and other
resouces inherited from the main Emulab site. In addition to raw
resources, PhantomNet provides configuration directives and scripts to
assist researchers in setting up their mobility experiments. Users
specify their experiment via Emulab NS file templates augmented with
PhantomNet-specific functionality. In complement to these template NS
files, PhantomNet does the work of configuring the EPC software
components to operate within the underlying Emulab environment.

The PhantomNet facility is built on top of
@hyperlink["http://www.emulab.net/"]{Emulab} and is run by the
@hyperlink["http://www.flux.utah.edu"]{Flux Research Group}, part of the
@hyperlink["http://www.cs.utah.edu/"]{School of Computing} at the
@hyperlink["http://www.utah.edu/"]{University of Utah}.

@table-of-contents[]

@include-section["getting-started.scrbl"]
@include-section["users.scrbl"]
@include-section["repeatable-research.scrbl"]
@include-section["creating-profiles.scrbl"]
@include-section["basic-concepts.scrbl"]
@include-section["geni-lib.scrbl"]
@include-section["advanced-storage.scrbl"]
@include-section["advanced-topics.scrbl"]
@include-section["hardware.scrbl"]
@include-section["planned.scrbl"]
@include-section["getting-help.scrbl"]
